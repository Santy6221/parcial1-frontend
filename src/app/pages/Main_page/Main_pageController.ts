import { indexOf } from "lodash";

export class Main_pageController {
    view: any;
    component = {
        id: 'main-page',
        view: 'Main_Page.html',
        style: 'Main_Page.scss',
        start: () => { this.start(); }
    }

    constructor(){
        this.loadView();
    }

    /**
     * Inicializa la pagina y lleva a cabo todo el codigo typescript
     * 
     */
    start(){
        const cover: any = this.findInsideMe(".cover");
        const desc: any = this.findInsideMe(".text1");
        const title: any =this.findInsideMe(".name");
        const self: any = this.findInsideMe("#myself")
        const skills: any = this.findInsideMe("#my-skills");
        const skill: any = this.findInsideMe(".skill", true);
        const resume: any = this.findInsideMe(".resume-reference", true)
        const resume_media: any = this.findInsideMe("video", true)
        const work: any = this.findInsideMe("#my-work");
        const clients: any = this.findInsideMe("#clients");
        const client: any = this.findInsideMe(".client", true);
        const sfx: any = this.findInsideMe(".sfx");
        const navbar: any = this.findInsideMe(".navelement", true)
        const contact: any = this.findInsideMe("#contact-me");
        // const footer: any = this.findInsideMe("#footer");
        const light_blue = '#94CEF2';
        const dark_blue = '#71BBD9';
        const yellow = '#F2CB57';
        const dark_red = '#A64833';
        const red =  '#BF4539';
        const bg_color = '#f5f5f5';

        /**
         * Espera a que los contenidos de la pagina se carguen y desencadena el cambio de
         * la propiedad de visibilidad de los elementos
         * @params callback
         */
        function onReady(callback:any) {
            var intervalId = window.setInterval(function() {
              if (document.getElementsByTagName('body')[0] !== undefined) {
                window.clearInterval(intervalId);
                callback.call(this);
              }
            }, 1000);
        }
        
        
        /**
         * cambia la propiedad de visisibilidad (display) entre block y none
         * 
         * @params selector, visible
         * donde el selector es el selector del html al que se le va a aplicar el cambio
         * y visibilidad es la propiedad de visibilidad de este
         */
        function setVisible(selector:any, visible:any) {
            document.querySelector(selector).style.display = visible ? 'block' : 'none';
        }


        
        onReady(function() {
            setVisible('#main-page', true);
            setVisible('.loader-container', false);
            // setScroll('*');

        });

        navbar.forEach((element: any) => {
            element.addEventListener('mouseenter', (e: any) =>{
                    element.style.color=bg_color;

                    switch(indexOf(navbar, element)){
                        case 0:
                            element.style.backgroundColor=dark_blue;
                        break;
                        case 1:
                            element.style.backgroundColor=yellow;
                        break;
                        case 2:
                            element.style.backgroundColor=red;
                        break;
                        case 3:
                            element.style.backgroundColor=yellow;
                        break;
                    }
            });
            element.addEventListener('mouseleave', (e: any) =>{
                element.style.backgroundColor=bg_color;
                element.style.color=dark_blue;
            })
        });

        window.onscroll = (_:any) => {
            const percent = (window.scrollY / window.innerHeight) * 100;
            //console.log(window.scrollY);
            desc.style.opacity = `${1-percent/20}`;
            title.style.opacity = `${1-percent/20}`;
            if(percent <= 26){
                self.style.position = 'sticky';
                self.style.bottom = 0;
                //console.log(`${percent}%`);
                cover.style.right = `${percent}%`;
                self.style.backgroundColor = bg_color;
                
            }else{
                self.style.backgroundColor = dark_blue;
                // self.style.display = `none`;
            }

            if(percent > 26 && percent <= 80){
                skills.style.backgroundColor = bg_color;
            }else{
                skills.style.backgroundColor = yellow;

            }

            if(percent > 80 && percent <= 190){
                work.style.backgroundColor = bg_color;
                // console.log(percent);
            }else{
                work.style.backgroundColor = red;
            }

            if(percent > 190){
                // console.log(percent);
                clients.style.backgroundColor = bg_color;
            }else{
                clients.style.backgroundColor = yellow;
            }

            
 
            resume.forEach((element: any) => {

                element.addEventListener('mouseenter', (e:any) =>{
                    //console.log(typeof(resume_media))
                    resume_media[indexOf(resume, element)].play();
                    //console.log(indexOf(resume, element));
                });

                element.addEventListener('mouseleave', (e:any) =>{
                    //console.log(typeof(resume_media))
                    resume_media[indexOf(resume, element)].pause();
                    //console.log(indexOf(resume, element));
                });

            });

            skill.forEach((element : any) => {
                element.addEventListener('mouseenter', (e : any) =>{
                    element.style.transform = 'translateY(-10px)';
                    element.style.backgroundColor = light_blue;
                    sfx.play();
                    //console.log('Hovering skill');
                });
                element.addEventListener('mouseleave', (e : any) =>{
                    element.style.transform = 'translateY(10px)';
                    element.style.backgroundColor = 'transparent';
                    sfx.pause();
                    //console.log('Hovering skill');
                });
            });

            client.forEach((element : any) => {
                element.addEventListener('mouseenter', (e : any) =>{
                    element.style.transform = 'translateY(-10px)';
                    element.style.backgroundColor = light_blue;
                    sfx.play();
                    // console.log('Hovering skill');
                });
                element.addEventListener('mouseleave', (e : any) =>{
                    element.style.transform = 'translateY(10px)';
                    element.style.backgroundColor = 'transparent';
                    sfx.pause();
                    // console.log('Hovering skill');
                });
            });

            
        
        }
    }

    /**
     * Encuentra un elemento dentro del documento html
     * 
     * @param selector Selector css del elemento
     * @param all si hay mas de un selector igual se usa true
     */
    findInsideMe(selector: string, all = false) {
        const query = `#app #${this.component.id} ${selector}`;
        console.log(query);
        if (!all) {
            return document.querySelector(query);
        } else {
            return document.querySelectorAll(query);

        }
    }

    loadView() {
        require(`./${this.component.style}`);
        this.view = require(`./${this.component.view}`);
    }

    getView(): [string, DocumentFragment] {
        return [this.component.id, document.createRange().createContextualFragment(this.view)];
    }
}